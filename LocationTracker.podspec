Pod::Spec.new do |s|

# 1
s.platform = :ios
s.ios.deployment_target = '9.3'
s.name = "LocationTrackerDemo"
s.summary = "Tracking your current location."
s.requires_arc = true

# 2
s.version = "0.1.0"

# 3
s.license = { :type => "MIT", :file => "LICENSE" }

# 4 - Replace with your name and e-mail address
s.author = { "Rakesh Kumar" => "rakesh@clicklabs.in" }

# For example,
# s.author = { "Joshua Greene" => "jrg.developer@gmail.com" }


# 5 - Replace this URL with your own Github page's URL (from the address bar)
s.homepage = "https://bitbucket.org/rakeshclabs/locationtrackerdemo"

# For example,
# s.homepage = "https://github.com/JRG-Developer/RWPickFlavor"


# 6 - Replace this URL with your own Git URL from "Quick Setup"
s.source = { :git => "https://rakeshclabs@bitbucket.org/rakeshclabs/locationtrackerdemo.git", :tag => "#{s.version}"}

# For example,
# s.source = { :git => "https://github.com/JRG-Developer/RWPickFlavor.git", :tag => "#{s.version}"}


# 7
s.framework = "UIKit"
s.dependency 'CocoaMQTT'

# 8
s.source_files = "LocationTrackerDemo/**/*.{swift}"

# 9
s.resources = "LocationTrackerDemo/**/*.{png,jpeg,jpg,storyboard,xib}"
end