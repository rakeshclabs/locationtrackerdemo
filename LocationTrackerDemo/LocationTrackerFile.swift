

//
//  LocationHandler.swift
//  Tookan
//
//  Created by Click Labs on 8/13/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit
import SystemConfiguration

class LocationTrackerFile:NSObject, CLLocationManagerDelegate {
    
    var myLastLocation: CLLocation!
    var myLocation: CLLocation!
    var myLocationAccuracy: CLLocationAccuracy!
    var locationUpdateTimer: NSTimer!
    var locationManager:CLLocationManager!
    var speed:Float = 0
    var slotTime = 5.0
    var maxSpeed:Float = 30.0
    var maxAccuracy = 20.0
    var maxDistance = 20.0
    var bgTask: BackgroundTaskManager?
    var hasLocationTrackingStarted = false
    private static let locationManagerObj = CLLocationManager()
    private static let locationTracker = LocationTrackerFile()
    
    override init() {
        super.init()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LocationTrackerFile.applicationEnterBackground), name: UIApplicationDidEnterBackgroundNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LocationTrackerFile.appEnterInTerminateState), name: UIApplicationWillTerminateNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LocationTrackerFile.enterInForegroundFromBackground), name: UIApplicationWillEnterForegroundNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LocationTrackerFile.becomeActive), name: UIApplicationDidBecomeActiveNotification, object: nil)
    }
    
    
    class func sharedInstance() -> LocationTrackerFile {
        return locationTracker
    }
    
    class func sharedLocationManager() -> CLLocationManager {
        return locationManagerObj
    }
    
    func applicationEnterBackground() {
        self.setLocationUpdate()
        self.bgTask = BackgroundTaskManager.sharedBackgroundTaskManager()
        self.bgTask!.beginNewBackgroundTask()
        NSUserDefaults.standardUserDefaults().setValue("Background", forKey: USER_DEFAULT.applicationMode)
        self.updateLocationToServer()
        if(self.locationUpdateTimer != nil) {
            self.locationUpdateTimer.invalidate()
            self.locationUpdateTimer = nil
        }
        self.locationUpdateTimer = NSTimer.scheduledTimerWithTimeInterval(slotTime, target: self, selector: #selector(LocationTrackerFile.updateLocationToServer), userInfo: nil, repeats: true)
    }
    
    func enterInForegroundFromBackground(){
        NSUserDefaults.standardUserDefaults().setValue("Foreground", forKey: USER_DEFAULT.applicationMode)
        self.updateLocationToServer()
        if(self.locationUpdateTimer != nil) {
            self.locationUpdateTimer.invalidate()
            self.locationUpdateTimer = nil
        }
        self.locationUpdateTimer = NSTimer.scheduledTimerWithTimeInterval(self.slotTime, target: self, selector: #selector(LocationTrackerFile.updateLocationToServer), userInfo: nil, repeats: true)
    }
    
    func appEnterInTerminateState() {
        NSUserDefaults.standardUserDefaults().setValue("Terminate", forKey: USER_DEFAULT.applicationMode)
        if(self.locationManager != nil) {
            self.locationManager.stopUpdatingLocation()
            self.locationManager.startMonitoringSignificantLocationChanges()
        }
    }
    
    func becomeActive() {
        if(self.locationManager == nil) {
            self.restartLocationUpdates()
        }
    }

    
    func setLocationUpdate() {
        MqttClass.sharedInstance.mqttSetting()
        MqttClass.sharedInstance.connectToServer()
        if(self.locationManager != nil) {
            self.locationManager.stopMonitoringSignificantLocationChanges()
        }
        locationManager = LocationTrackerFile.sharedLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        locationManager.activityType = CLActivityType.AutomotiveNavigation
        locationManager.pausesLocationUpdatesAutomatically = false
        locationManager.distanceFilter = maxDistance //kCLDistanceFilterNone//
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func restartLocationUpdates() {
        if(self.locationUpdateTimer != nil) {
            self.locationUpdateTimer.invalidate()
            self.locationUpdateTimer = nil
        }
        if(self.locationManager != nil) {
            self.locationManager.stopMonitoringSignificantLocationChanges()
        }
                
        setLocationUpdate()
        self.updateLocationToServer()
        self.locationUpdateTimer = NSTimer.scheduledTimerWithTimeInterval(slotTime, target: self, selector: #selector(LocationTrackerFile.updateLocationToServer), userInfo: nil, repeats: true)
    }
    
    func startLocationTracking() {
        
        setLocationUpdate()
        if(self.locationUpdateTimer != nil) {
            self.locationUpdateTimer?.invalidate()
            self.locationUpdateTimer = nil
        }
        self.updateLocationToServer()
        self.locationUpdateTimer = NSTimer.scheduledTimerWithTimeInterval(self.slotTime, target: self, selector: #selector(LocationTrackerFile.updateLocationToServer), userInfo: nil, repeats: true)
    }
    
    func stopLocationTracking() {
        if(self.locationUpdateTimer != nil) {
            self.locationUpdateTimer.invalidate()
            self.locationUpdateTimer = nil
        }
        MqttClass.sharedInstance.disconnect()
        let locationManager: CLLocationManager = LocationTrackerFile.sharedLocationManager()
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.bgTask = BackgroundTaskManager.sharedBackgroundTaskManager()
        self.bgTask!.beginNewBackgroundTask()
        if locations.last != nil {
            self.myLocation = locations.last! as CLLocation
            self.myLocationAccuracy = self.myLocation.horizontalAccuracy
            self.applyFilterOnGetLocation()
        }
    }
    
    func applyFilterOnGetLocation() {
        if self.myLocation != nil  {
            var locationArray = NSMutableArray()
            if let array = NSUserDefaults.standardUserDefaults().objectForKey(USER_DEFAULT.locationArray) as? NSMutableArray {
                locationArray = NSMutableArray(array: array)
            }
            if hasLocationTrackingStarted == true {
                if(self.myLocationAccuracy < maxAccuracy){
                    if(self.myLastLocation == nil) {
                        var myLocationToSend = NSMutableDictionary()
                        let timestamp = String().getUTCDateString
                        myLocationToSend = ["lat" : myLocation!.coordinate.latitude as Double,"lng" :myLocation!.coordinate.longitude as Double, "tm_stmp" : timestamp, "bat_lvl" : UIDevice.currentDevice().batteryLevel * 100, "gps":1,"acc":(self.myLocationAccuracy != nil ? self.myLocationAccuracy! : 300), "d_acc":"1"]
                        self.addFilteredLocationToLocationArray(myLocationToSend)
                        self.myLastLocation = self.myLocation
                    } else {
                        if(self.getSpeed() < maxSpeed) {
                            var myLocationToSend = NSMutableDictionary()
                            let timestamp = String().getUTCDateString
                            myLocationToSend = ["lat" : myLocation!.coordinate.latitude as Double,"lng" :myLocation!.coordinate.longitude as Double, "tm_stmp" : timestamp, "bat_lvl" : UIDevice.currentDevice().batteryLevel * 100, "gps":1,"acc":(self.myLocationAccuracy != nil ? self.myLocationAccuracy! : 300), "d_acc":"1"]
                            self.addFilteredLocationToLocationArray(myLocationToSend)
                            self.myLastLocation = self.myLocation
                        }
                    }
                }
            }
            
            if(NSUserDefaults.standardUserDefaults().valueForKey(USER_DEFAULT.applicationMode) != nil && (NSUserDefaults.standardUserDefaults().valueForKey(USER_DEFAULT.applicationMode) as! String == "Background" || NSUserDefaults.standardUserDefaults().valueForKey(USER_DEFAULT.applicationMode) as! String == "Terminate")) {
                if(NSUserDefaults.standardUserDefaults().boolForKey(USER_DEFAULT.isHitInProgress) == false) {
                    if locationArray.count >= 5 {
                        let locationString = locationArray.jsonString
                        sendRequestToServer(locationString)
                    }
                }
            }
        }
    }
    
    func addFilteredLocationToLocationArray(myLocationToSend:NSMutableDictionary) {
        if(hasLocationTrackingStarted == true){
            var locationArray = NSMutableArray()
            if let array = NSUserDefaults.standardUserDefaults().objectForKey(USER_DEFAULT.locationArray) as? NSMutableArray {
                locationArray = NSMutableArray(array: array)
            }
            if(locationArray.count >= 1000) {
                locationArray.removeObjectAtIndex(0)
            }
            locationArray.addObject(myLocationToSend)
            NSUserDefaults.standardUserDefaults().setObject(locationArray, forKey: USER_DEFAULT.locationArray)
        }
    }
    
    
    func updateLocationToServer() {
        var locationArray = NSMutableArray()
        if let array = NSUserDefaults.standardUserDefaults().objectForKey(USER_DEFAULT.locationArray) as? NSMutableArray {
            locationArray = NSMutableArray(array: array)
        }
        if IJReachability.isConnectedToNetwork(){
            if(NSUserDefaults.standardUserDefaults().boolForKey(USER_DEFAULT.isHitInProgress) == false) {
                if locationArray.count > 0 {
                    let locationString = locationArray.jsonString
                    sendRequestToServer(locationString)
                }
            }
        }
    }
    
    func sendRequestToServer(locationString:String) {
        if(hasLocationTrackingStarted == true) {
            MqttClass.sharedInstance.sendLocation(locationString)//MQTT
        }
    }
    
    func updateLastSavedLocationOnServer() {
        if(hasLocationTrackingStarted == true) {
            var locationArray = NSMutableArray()
            if let array = NSUserDefaults.standardUserDefaults().objectForKey(USER_DEFAULT.locationArray) as? NSMutableArray {
                locationArray = NSMutableArray(array: array)
            }
            self.setLocationUpdate()
            if(NSUserDefaults.standardUserDefaults().boolForKey(USER_DEFAULT.isHitInProgress) == false) {
                if(locationArray.count > 0) {
                    sendRequestToServer(locationArray.jsonString)
                } else {
                    var myLocationToSend = NSMutableDictionary()
                    myLocationToSend = ["bat_lvl" : UIDevice.currentDevice().batteryLevel * 100]
                    let highLocationArray = NSMutableArray()
                    highLocationArray.addObject(myLocationToSend)
                    let locationString = highLocationArray.jsonString
                    sendRequestToServer(locationString)
                }
            }
        }
    }
    
    func getSpeed() -> Float {
        if(myLastLocation != nil) {
            let time = self.myLocation.timestamp.timeIntervalSinceDate(myLastLocation.timestamp)
            let distance:CLLocationDistance = myLocation.distanceFromLocation(myLastLocation)
            if(distance > 200) {
                self.locationManager.stopUpdatingLocation()
                if let json = NetworkingHelper.sharedInstance.getLatLongFromDirectionAPI("\(myLastLocation.coordinate.latitude),\(myLastLocation.coordinate.longitude)", destination: "\(myLocation.coordinate.latitude),\(myLocation.coordinate.longitude)") {
                    UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                    if let routes = json["routes"] {
                        if(routes.count > 0) {
                            if let legs = routes[0]["legs"]!{
                                if(legs.count > 0) {
                                    if let _ = legs[0]["distance"]!!["value"] as? Int {
                                        //                                if(distance < 200) {
                                        if let polyline = routes[0]["overview_polyline"]!!["points"] as? String {
                                            let locations = NetworkingHelper.sharedInstance.decodePolylineForCoordinates(polyline)
                                            for i in (0..<locations.count) {
                                                var myLocationToSend = NSMutableDictionary()
                                                let timestamp = String().getUTCDateString
                                                myLocationToSend = ["lat" : locations[i].coordinate.latitude as Double,"lng" :locations[i].coordinate.longitude as Double, "tm_stmp" : timestamp, "bat_lvl" : UIDevice.currentDevice().batteryLevel * 100, "gps":1,"acc":(self.myLocationAccuracy != nil ? self.myLocationAccuracy! : 300), "d_acc":"1","status":"1"]
                                               
                                                self.addFilteredLocationToLocationArray(myLocationToSend)
                                                self.myLastLocation = CLLocation(latitude: locations[i].coordinate.latitude, longitude: locations[i].coordinate.longitude)
                                                self.setLocationUpdate()
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                        self.setLocationUpdate()
                        speed = Float(distance) / Float(time)
                        if(speed > 0) {
                            return speed
                        }
                        return maxSpeed
                    }
                } else {
                    UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                    self.setLocationUpdate()
                    speed = Float(distance) / Float(time)
                    if(speed > 0) {
                        return speed
                    }
                    return maxSpeed
                }
                return maxSpeed
            } else {
//                if(distance < 20) {
//                    return 25.0
//                } else {
                    speed = Float(distance) / Float(time)
                if(speed > 0) {
                    return speed
                }
                return maxSpeed
            }
        }
        return 0.0
    }
    
    func isAllPermissionAuthorized() -> (Bool, String) {
        //We have to make sure that the Background App Refresh is enable for the Location updates to work in the background.
        if(UIApplication.sharedApplication().backgroundRefreshStatus == UIBackgroundRefreshStatus.Denied) {
            return (false,"The app doesn't work without the Background App Refresh enabled.")
        } else if (UIApplication.sharedApplication().backgroundRefreshStatus == UIBackgroundRefreshStatus.Restricted) {
            return (false,"The app doesn't work without the Background App Refresh enabled.")
        } else {
            return self.isAppLocationEnabled()
        }
    }
    
    private func isAppLocationEnabled() -> (Bool,String) {
        if CLLocationManager.locationServicesEnabled() == false {
            return (false,"Background Location Access Disabled")
        } else {
            let authorizationStatus = CLLocationManager.authorizationStatus()
            if authorizationStatus == CLAuthorizationStatus.Denied || authorizationStatus == CLAuthorizationStatus.Restricted {
                return (false,"Background Location Access Disabled")
            } else {
                return (true,"")
            }
        }
    }
}

